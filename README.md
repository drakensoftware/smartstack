# SmartStack
## Start your project with a Stack that works

Are you ovelwhelmed, or even having laziness to start a project, because you do not know which stack you should use?

SmartStack is here to solve your problems.

Just run "npx smartstack PROJECT_NAME" and SmartStack will create your project, ready to use, with an optimous Stack that is proven and works.

## Current supported languages and frameworks

### Typescript:
- **Web app**: (WebStorm) NextJS + ESLint + Jest
- **Desktop app**: (WebStorm) NextJS + Electron + ESLint + Jest
- **NodeJS app**: (WebStorm) Node + ESLint + Jest
- **Solidity**: (VSCode) HardHat + Ethers + TypeChain + Mocha
