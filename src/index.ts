#!/usr/bin/env node

import cliSelect from "cli-select"
import { readdirSync } from "fs"
import * as path from "path"
import Commander from "commander"
import * as fs from "fs"
const program = Commander.program

program
	.name("SmartStack")
	.description("Start a project with a proven stack")
	.version("1.0.5")

program.argument("<project>", "Name of the project")
	.action(async (str, _options) => {
		await createProject(str)
	})

program.parse()

function unpack(projectName: string, template: string){
	copyDir(path.resolve(__dirname, "../templates", template), projectName)
}

function enumTemplates(): Array<string>{
	return readdirSync(path.resolve(__dirname, "../templates"), { withFileTypes: true })
		.filter(dirent => dirent.isDirectory())
		.map(dirent => dirent.name)
}

async function createProject(projectName: string): Promise<void>{
	console.log("Choose the template to use:\n")

	const selected = await cliSelect({
		values: enumTemplates()
	})

	console.log(`Unpacking ${selected.value}...`)
	unpack(projectName, selected.value)
}

function copyDir(src, dest) {
	const exists = fs.existsSync(src)
	const stats = exists && fs.statSync(src)
	const isDirectory = exists && stats.isDirectory()
	if (isDirectory) {
		fs.mkdirSync(dest)
		fs.readdirSync(src).forEach(function (childItemName) {
			copyDir(path.join(src, childItemName),
				path.join(dest, childItemName))
		})
	} else {
		fs.copyFileSync(src, dest)
	}
}
