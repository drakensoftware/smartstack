// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.21;

import "hardhat/console.sol";


contract Counter {
    uint256 count = 0;

    event CountedTo(uint256 number);

    function getCount() public view returns (uint256) {
        return count;
    }

    function countUp() public returns (uint256) {
        console.log("countUp: count =", count);
        require(count < type(uint256).max, "Uint256 overflow");
        uint256 newCount = count + 1;

        count = newCount;

        emit CountedTo(count);
        return count;
    }

    function countDown() public returns (uint256) {
        console.log("countDown: count =", count);
        require(count>0, "Uint256 underflow");
        uint256 newCount = count - 1;

        count = newCount;

        emit CountedTo(count);
        return count;
    }
}
