import '@nomiclabs/hardhat-truffle5'
import '@typechain/hardhat'
import '@nomicfoundation/hardhat-ethers'
import { HardhatUserConfig } from 'hardhat/types'


const config: HardhatUserConfig = {
  defaultNetwork: 'hardhat',
  networks: {
    deploy_network: {
      url: process.env.DEPLOY_NODE_URL?? 'https://eth-sepolia.g.alchemy.com/v2/demo',
      accounts: process.env.DEPLOY_PRIVATE_KEY ? [process.env.DEPLOY_PRIVATE_KEY] : 
                process.env.DEPLOY_MNEMONIC ? { mnemonic: process.env.DEPLOY_MNEMONIC } :
                { mnemonic: 'test test test test test test test test test test test test' }
    }
  },
  solidity: {
    compilers: [{ version: '0.8.21', settings: {} }],
  },
  typechain: {
    target: 'truffle-v5',
  },
}

export default config
