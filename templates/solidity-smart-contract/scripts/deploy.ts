import { ethers } from 'hardhat'

async function main() {
    const deploy = await ethers.deployContract('Counter', [], {})
    console.log(`Contract deployed to ${await deploy.getAddress()}`)
}

main().catch((error) => {
    console.error(error)
    process.exitCode = 1
})