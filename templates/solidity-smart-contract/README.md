# Solidity smart contract template

## Configure

This project should be run in VSCode. To configure it:

- First, you should run `npm install`.
- Then, run `npm run compile`.

That way all types should be generated and recognized by VSCode, and VSCode should not report any error.

## Testing

To test the smart contract, you may run `npm run test`.
You may also run `Test` configuration in VSCode, which allow debugging your tests.

## Deploying

For deploying, you need to configure the following environment variables:

- `DEPLOY_RPC_URL`, which points to the RPC URL of the network you'd like to deploy in
- `DEPLOY_PRIVATE_KEY` or `DEPLOY_MNEMONIC`, the private key or mnemonic used to deploy the contract. If both are bot specified, deployment will be made by the private key.

You may also run `Deploy` configuration in VSCode, which already have environment
variables configured. If you wish to use it, you may edit the mnemonic and RPC url
in `.vscode/launch.json`.

If you do not specify any rpc or mnemonic, the contract will be developed to Sepolia testnet,
to the address `0x5F60A51A1ab2d63D235349C7093445B556734C03`.