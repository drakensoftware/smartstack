import BN from 'bn.js'
import chai from 'chai'
import chaiAsPromised from 'chai-as-promised'
import { artifacts, contract } from 'hardhat'

// chai.use(solidity);
chai.use(chaiAsPromised).should()
const { expect } = chai

import { CounterContract, CounterInstance } from '../typechain-types'
const Counter: CounterContract = artifacts.require('Counter')

contract('Counter', (accounts: string[]) => {
    let counter: CounterInstance

    beforeEach(async () => {
        counter = await Counter.new()
        const initialCount = await counter.getCount()
        expect(initialCount.eq(new BN(0))).true
        counter = await Counter.at(counter.address)
    })

    // 4
    describe('count up', async () => {
        it('should count up', async () => {
            await counter.countUp()
            const count = await counter.getCount()
            expect(count.eq(new BN(1))).true
        })
    })

    describe('count down', async () => {
        it('should fail due to underflow exception', async () => {
            await expect(counter.countDown()).to.eventually.be.rejectedWith(Error, 'Uint256 underflow')
        })

        it('should count down', async () => {
            await counter.countUp()

            await counter.countDown()
            const count = await counter.getCount()
            expect(count.eq(new BN(0))).true
        })
    })
})
